
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package ru.catharsisdream.mylittlestorymod.init;

import ru.catharsisdream.mylittlestorymod.world.features.ores.SilverOreFeature;
import ru.catharsisdream.mylittlestorymod.MylittlestorymodMod;

import net.minecraftforge.registries.RegistryObject;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.fml.common.Mod;

import net.minecraft.world.level.levelgen.feature.Feature;

@Mod.EventBusSubscriber
public class MylittlestorymodModFeatures {
	public static final DeferredRegister<Feature<?>> REGISTRY = DeferredRegister.create(ForgeRegistries.FEATURES, MylittlestorymodMod.MODID);
	public static final RegistryObject<Feature<?>> SILVER_ORE = REGISTRY.register("silver_ore", SilverOreFeature::feature);
}
