
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package ru.catharsisdream.mylittlestorymod.init;

import ru.catharsisdream.mylittlestorymod.item.SilverToolsShovelItem;
import ru.catharsisdream.mylittlestorymod.item.SilverToolsPickaxeItem;
import ru.catharsisdream.mylittlestorymod.item.SilverToolsHoeItem;
import ru.catharsisdream.mylittlestorymod.item.SilverToolsAxeItem;
import ru.catharsisdream.mylittlestorymod.item.SilverSwordItem;
import ru.catharsisdream.mylittlestorymod.item.SilverIngotItem;
import ru.catharsisdream.mylittlestorymod.item.InfernalSwordItem;
import ru.catharsisdream.mylittlestorymod.MylittlestorymodMod;

import net.minecraftforge.registries.RegistryObject;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.DeferredRegister;

import net.minecraft.world.level.block.Block;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.BlockItem;

public class MylittlestorymodModItems {
	public static final DeferredRegister<Item> REGISTRY = DeferredRegister.create(ForgeRegistries.ITEMS, MylittlestorymodMod.MODID);
	public static final RegistryObject<Item> SILVER_ORE = block(MylittlestorymodModBlocks.SILVER_ORE, MylittlestorymodModTabs.TAB_MY_LITTLE_STORY_MOD);
	public static final RegistryObject<Item> SILVER_INGOT = REGISTRY.register("silver_ingot", () -> new SilverIngotItem());
	public static final RegistryObject<Item> METAL_BLOCK = block(MylittlestorymodModBlocks.METAL_BLOCK, MylittlestorymodModTabs.TAB_MY_LITTLE_STORY_MOD);
	public static final RegistryObject<Item> INFERNAL_SWORD = REGISTRY.register("infernal_sword", () -> new InfernalSwordItem());
	public static final RegistryObject<Item> SILVER_SWORD = REGISTRY.register("silver_sword", () -> new SilverSwordItem());
	public static final RegistryObject<Item> SILVER_TOOLS_PICKAXE = REGISTRY.register("silver_tools_pickaxe", () -> new SilverToolsPickaxeItem());
	public static final RegistryObject<Item> SILVER_TOOLS_AXE = REGISTRY.register("silver_tools_axe", () -> new SilverToolsAxeItem());
	public static final RegistryObject<Item> SILVER_TOOLS_SHOVEL = REGISTRY.register("silver_tools_shovel", () -> new SilverToolsShovelItem());
	public static final RegistryObject<Item> SILVER_TOOLS_HOE = REGISTRY.register("silver_tools_hoe", () -> new SilverToolsHoeItem());

	private static RegistryObject<Item> block(RegistryObject<Block> block, CreativeModeTab tab) {
		return REGISTRY.register(block.getId().getPath(), () -> new BlockItem(block.get(), new Item.Properties().tab(tab)));
	}
}
