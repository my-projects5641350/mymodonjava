
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package ru.catharsisdream.mylittlestorymod.init;

import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.CreativeModeTab;

public class MylittlestorymodModTabs {
	public static CreativeModeTab TAB_MY_LITTLE_STORY_MOD;

	public static void load() {
		TAB_MY_LITTLE_STORY_MOD = new CreativeModeTab("tabmy_little_story_mod") {
			@Override
			public ItemStack makeIcon() {
				return new ItemStack(MylittlestorymodModItems.INFERNAL_SWORD.get());
			}

			@Override
			public boolean hasSearchBar() {
				return false;
			}
		};
	}
}
