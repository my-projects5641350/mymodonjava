
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package ru.catharsisdream.mylittlestorymod.init;

import ru.catharsisdream.mylittlestorymod.block.SilverOreBlock;
import ru.catharsisdream.mylittlestorymod.block.MetalBlockBlock;
import ru.catharsisdream.mylittlestorymod.MylittlestorymodMod;

import net.minecraftforge.registries.RegistryObject;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.DeferredRegister;

import net.minecraft.world.level.block.Block;

public class MylittlestorymodModBlocks {
	public static final DeferredRegister<Block> REGISTRY = DeferredRegister.create(ForgeRegistries.BLOCKS, MylittlestorymodMod.MODID);
	public static final RegistryObject<Block> SILVER_ORE = REGISTRY.register("silver_ore", () -> new SilverOreBlock());
	public static final RegistryObject<Block> METAL_BLOCK = REGISTRY.register("metal_block", () -> new MetalBlockBlock());
}
