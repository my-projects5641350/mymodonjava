
package ru.catharsisdream.mylittlestorymod.item;

import ru.catharsisdream.mylittlestorymod.init.MylittlestorymodModTabs;

import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.item.Tier;
import net.minecraft.world.item.SwordItem;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Item;

public class InfernalSwordItem extends SwordItem {
	public InfernalSwordItem() {
		super(new Tier() {
			public int getUses() {
				return 1000;
			}

			public float getSpeed() {
				return 4f;
			}

			public float getAttackDamageBonus() {
				return 8f;
			}

			public int getLevel() {
				return 1;
			}

			public int getEnchantmentValue() {
				return 2;
			}

			public Ingredient getRepairIngredient() {
				return Ingredient.of(new ItemStack(Items.BLAZE_ROD));
			}
		}, 3, -2f, new Item.Properties().tab(MylittlestorymodModTabs.TAB_MY_LITTLE_STORY_MOD).fireResistant());
	}
}
