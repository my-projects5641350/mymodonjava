
package ru.catharsisdream.mylittlestorymod.item;

import ru.catharsisdream.mylittlestorymod.init.MylittlestorymodModTabs;

import net.minecraft.world.item.Rarity;
import net.minecraft.world.item.Item;

public class SilverIngotItem extends Item {
	public SilverIngotItem() {
		super(new Item.Properties().tab(MylittlestorymodModTabs.TAB_MY_LITTLE_STORY_MOD).stacksTo(64).rarity(Rarity.COMMON));
	}
}
