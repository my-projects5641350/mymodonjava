
package ru.catharsisdream.mylittlestorymod.item;

import ru.catharsisdream.mylittlestorymod.init.MylittlestorymodModTabs;
import ru.catharsisdream.mylittlestorymod.init.MylittlestorymodModItems;

import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.item.Tier;
import net.minecraft.world.item.SwordItem;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Item;

public class SilverSwordItem extends SwordItem {
	public SilverSwordItem() {
		super(new Tier() {
			public int getUses() {
				return 500;
			}

			public float getSpeed() {
				return 4f;
			}

			public float getAttackDamageBonus() {
				return 2.5f;
			}

			public int getLevel() {
				return 2;
			}

			public int getEnchantmentValue() {
				return 14;
			}

			public Ingredient getRepairIngredient() {
				return Ingredient.of(new ItemStack(MylittlestorymodModItems.SILVER_INGOT.get()));
			}
		}, 3, -2.4f, new Item.Properties().tab(MylittlestorymodModTabs.TAB_MY_LITTLE_STORY_MOD));
	}
}
