
package ru.catharsisdream.mylittlestorymod.item;

import ru.catharsisdream.mylittlestorymod.init.MylittlestorymodModTabs;
import ru.catharsisdream.mylittlestorymod.init.MylittlestorymodModItems;

import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.item.Tier;
import net.minecraft.world.item.PickaxeItem;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Item;

public class SilverToolsPickaxeItem extends PickaxeItem {
	public SilverToolsPickaxeItem() {
		super(new Tier() {
			public int getUses() {
				return 250;
			}

			public float getSpeed() {
				return 6f;
			}

			public float getAttackDamageBonus() {
				return 0f;
			}

			public int getLevel() {
				return 3;
			}

			public int getEnchantmentValue() {
				return 14;
			}

			public Ingredient getRepairIngredient() {
				return Ingredient.of(new ItemStack(MylittlestorymodModItems.SILVER_INGOT.get()));
			}
		}, 1, 0.5f, new Item.Properties().tab(MylittlestorymodModTabs.TAB_MY_LITTLE_STORY_MOD));
	}
}
